# trouver le nombre d'éléments d'une lsite de manière récursive
# interdit d'utiliser len !
# la longeuur d'une liste est : 
#    0 si la liste est vide
#    sinon, 1 + la longueur de la suite de la liste

# 1ère solution : calcul du nombre d'éléments lors des dépilements
def lg_liste_A(liste: list) -> int:
    """
    renvoie la longeuur (nombre d'éléments)
    de la liste
    """
    if liste == []:
        return 0
    return 1 + lg_liste(liste[1:])

# autre solution : calcul du nombre d'éléments lors de l'empilement
def lg_liste_B(liste: list, lg: int) -> int:
    """
    renvoie la longeuur (nombre d'éléments)
    de la liste
    """
    if liste == []:
        return lg
    return lg_liste_B(liste[1:], lg + 1)


# QUESTION : quelle valeur faut-il donner au paramètre lg lorsqu'on appelle
# lg_liste_B ? 

# tests
lg_liste = lg_liste_A
assert lg_liste([]) == 0
assert lg_liste([0] * 100) == 100

lg_liste = lg_liste_B
assert lg_liste([], 0) == 0
assert lg_liste([0] * 100, 0) == 100




                 
