#
import random
def pile_ou_face():
    return random.choice((True, False))

def alea_pf_iter(dern: int) -> int:
    """
    renvoie un nombre pris aléatoirement dans l'intervalle
    entier [1 .. dern]. 
    dern doit ê une puissance de 2
    """
    prem = 1
    while prem < dern:
        if pile_ou_face():
            prem = (prem + dern) // 2 + 1
        else:
            dern = (prem + dern) // 2
    return prem


# tests : les fréquences des 16 nombres possibles doivent être proches de 1/16 
N = 16
nbre_essais = 100_000
resultats = [0] * nbre_essais
for i in range(nbre_essais):
    resultats[i] = alea_pf_iter(N)
frequences = [0] * 16
for x in range(16):
    frequences[x] = resultats.count(x+1) / nbre_essais
print("alea_pf_iter")
for i, f in enumerate(frequences):
    print(F"{i+1} : {f} ; ", end = "")
print()

    
# récursif, méthode avec premier - dernier
def alea_pf_rec_A(prem: int, dern: int) -> int:
    """
    prem - dern + 1 doit ê une puissance de 2
    """
    if prem == dern:
        return prem

    if pile_ou_face():
        return alea_pf_rec_A((prem + dern) // 2 + 1, dern)
    else:
        return alea_pf_rec_A(prem, (prem + dern) // 2)

# tests
N = 16
nbre_essais = 100_000
resultats = [0] * nbre_essais
for i in range(nbre_essais):
    resultats[i] = alea_pf_rec_A(1, N)
frequences = [0] * 16
for x in range(16):
    frequences[x] = resultats.count(x+1) / nbre_essais
print("alea_pf_rec_A")
for i, f in enumerate(frequences):
    print(F"{i+1} : {f} ; ", end = "")
print() 


# récursif,  méthode avec slices
def alea_pf_rec_B(intervalle: list) -> int:
    """
    la taille de l'intervalle doit ê une puissance de 2
    """
    if len(intervalle) == 1:
        return intervalle[0]
    if pile_ou_face():
        return alea_pf_rec_B(intervalle[:len(intervalle)//2])
    else:
        return alea_pf_rec_B(intervalle[len(intervalle)//2:])

# tests
N = 16
nbre_essais = 100_000
resultats = [0] * nbre_essais
for i in range(nbre_essais):
    resultats[i] = alea_pf_rec_B(list(range(1, N+1)))
frequences = [0] * 16
for x in range(16):
    frequences[x] = resultats.count(x+1) / nbre_essais
print("alea_pf_rec_B")
for i, f in enumerate(frequences):
    print(F"{i+1} : {f} ; ", end = "")
    




