def reco_palindrome(s: str) -> bool:
    """
    renvoie True si s est un palindrome, False dans le cas contraire
    """
    if s == "":
        return True
    return s[0] == s[-1] and reco_palindrome(s[1:-1])



assert reco_palindrome("kayak") is True
assert reco_palindrome("") is True
assert reco_palindrome("111") is True


def reco_palindrome_iter(s: str) -> bool:
    """
    renvoie True si s est un palindrome, False dans le cas contraire
    """
    i, j = 0, len(s) - 1
    while i < j:
        if s[i] != s[j]:
            return False
        i += 1
        j -= 1
    return True

assert reco_palindrome_iter("kayak") is True
assert reco_palindrome_iter("") is True
assert reco_palindrome_iter("111") is True



