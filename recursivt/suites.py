# calculer les termes de la suite ET les afficher avec une fonction récursive

"""
suite1:
    u(0) = 1
    u(n+1) = 2 * u(n) + 1
"""

def suite1_rec(der_term: int) -> int:
    if der_term == 0:
        print(1)
        return 1
    t = suite1_rec(der_term -1) * 2 + 1
    print(t)
    return t


# tests 
suite = suite1_rec
suite(7)

assert suite(7) == 255
assert suite(5) == 63
assert suite(0) == 1

# de manière iterative, avec une fonction annexe
print()

def suite1_iter(n: int) -> int:
    """
    calcule le N-ième terme de la suite1
    """
    u = 1
    for _ in range(n):
        u = 2 * u + 1
    return u

# tests 
suite = suite1_iter
assert suite(7) == 255
assert suite(5) == 63
assert suite(0) == 1

# affichage des termes

def affiche_suite(der_term: int):
    for terme in range(der_term + 1):
        print(suite1_iter(terme))


affiche_suite(7)
