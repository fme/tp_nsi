def affiche_de_0_a_N(N : int):
    """
    affiche les entiers de 0 à N inclus
    N doit être un entier positif
    """
    if N < 0:
        return
    affiche_de_0_a_N(N-1)
    print(N, end=" ")


def affiche_de_0_a_10(x: int):
    """
    affiche les entiers de x à 10 inclus
    """
    if x > 10:
        return 
    print(x, end=" ")
    affiche_de_0_a_10(x + 1)


affiche_de_0_a_N(10)
print()
affiche_de_0_a_10(0)


def affiche_multiples3(N: int):
    """
    affiche les multiples de 3, de 0 à N inclus
    N doit donc être multiple de 3.
    """
    if N < 0:
        return
    affiche_multiples3(N-3)
    print(N, end=" ")

# ou plus compact

def affiche_multiples3(N: int):
    assert N % 3 == 0
    if N >= 0:
        affiche_multiples3(N-3)
        print(N, end=" ")

print()
affiche_multiples3(30)
print()

# variante : 
# sans les print, seulement des return

def affiche_de_0_a_N_bis(N):
    if N == 0:
        return "0"
    return affiche_de_0_a_N_bis(N-1) + str(N)

print(affiche_de_0_a_N_bis(10))
print()
