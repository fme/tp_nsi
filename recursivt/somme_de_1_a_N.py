def somme_de_1_a_N(N):
    """
    renvoie la somme des entiers de 1 à N inclus
    """
    if N == 0:
        return N
    return somme_de_1_a_N(N-1) + N


assert somme_de_1_a_N(10) == 55
assert somme_de_1_a_N(0) == 0


## variante

def somme_de_1_a_N_bis(N, x):
    """
    renvoie la somme des entiers de 1 à N inclus
    """
    if x == N:
        return N
    return x + somme_de_1_a_N_bis(N, x+1)


assert somme_de_1_a_N_bis(10, 0) == 55
assert somme_de_1_a_N_bis(0, 0) == 0


