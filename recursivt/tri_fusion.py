def fusionner_A(G: list, D: list):
    """
    Renvoie la liste contenant les éléments de L1 et de L2, 
    triés dans l'ordre croissant. Les listes L1 et L2 
    doivent être elles-même triées dans l'ordre croissant.
    """
    R = list()
    g, d = 0, 0
    while g < len(G) and d < len(D):
        if G[g] < D[d]:
            R.append(G[g])
            g += 1
        else:
            R.append(D[d])
            d += 1
    if g < len(G):
        R.extend(G[g:])
    else:
        R.extend(D[d:])
    return R

# tests
fusionner = fusionner_A
assert fusionner([0, 1, 2, 3, 4], [3, 4]) == [0, 1, 2, 3, 3, 4, 4]
assert fusionner([],  [1, 2, 3]) == [1, 2, 3]
assert fusionner([2, 7, 9], []) == [2, 7, 9]
assert fusionner([], []) == []
assert fusionner([1, 1, 1], [2, 2, 2, 2]) == [1, 1, 1, 2, 2, 2, 2]
assert fusionner("bijoux", "effort") == ['b', 'e', 'f', 'f', 'i', 'j', 'o', 'o', 'r', 't', 'u', 'x']
# assert tri_fusion([0, 3, 1, 6, 7, 1, 0, 3, 7, 2]) ==\
#        [0, 0, 1, 1, 2, 3, 3, 6, 7, 7]

# autre version, qui recopie les deux listes dos-à-dos

def fusionner_B(G, D):
    T = G + [D[ i ] for i in range(len(D)-1, -1, -1)]
    F = [ None ] * len(T)
    ig = 0
    id = len(T) - 1
    for i in range(len(F)):
        if T[ ig ] < T[ id ]:
            F[ i ] = T[ ig ]
            ig += 1
        else:
            F[ i ] = T[ id ]
            id -= 1
    return F

# tests
fusionner = fusionner_B
assert fusionner([0, 1, 2, 3, 4], [3, 4]) == [0, 1, 2, 3, 3, 4, 4]
assert fusionner([],  [1, 2, 3]) == [1, 2, 3]
assert fusionner([2, 7, 9], []) == [2, 7, 9]
assert fusionner([], []) == []
assert fusionner([1, 1, 1], [2, 2, 2, 2]) == [1, 1, 1, 2, 2, 2, 2]
assert fusionner("bijoux", "effort") == ['b', 'e', 'f', 'f', 'i', 'j', 'o', 'o', 'r', 't', 'u', 'x']
#

