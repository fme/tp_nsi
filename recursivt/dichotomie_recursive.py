

# dichotomie avec tranches
# l'appel récursif est fait en passant comme argument la tranche de liste
# correspondant à la moitié de droite ou de gauche, selon l'issue du test 

def recherche_dichotomique_tranches(cible, liste):
    """
    Renvoie True si cible est dans liste, False dans le cas contraire
    """
    # pour suivre les arguments des appels récursifs
    print(F"recherche_dichotomique_tranches({cible}, {liste})")
    #
    if len(liste) == 0:
        return False
    milieu = len(liste) // 2 
    if cible == liste[milieu]:
        return True
    elif cible < liste[milieu]:
        return recherche_dichotomique_tranches(cible, liste[:milieu])
    else:
        return recherche_dichotomique_tranches(cible, liste[milieu + 1:])


recherche = recherche_dichotomique_tranches

assert recherche(0, list(range(10))) is True
assert recherche(9, list(range(10))) is True
assert recherche(10, list(range(10))) is False
assert recherche('a', list("abcdef")) is True
assert recherche('z', []) is False


# dichotomie avec indices premier et dernier
# l"appel récursif est fait avec la liste entière, mais en donnant comme
# argument l'indice du premier et du dernier de la moitié gauche ou droite,
# selon l'issue du test

def recherche_dichotomique(v, L, p, d):  # 
    """
    Recherche la valeur v dans liste L entre 
    les indices p (premier) et d (dernier) inclus.
    Renvoie False si v n'est pas dans L, True dans le
    cas contraire.
    """
    if p > d:
        return False
    m = (p + d) // 2
    if L[m] == v:
        return True
    elif v < L[m]:
        return recherche_dichotomique(v, L, p, m-1)
    else:
        return recherche_dichotomique(v, L, m+1, d)




recherche = recherche_dichotomique

assert recherche(0, list(range(10)), 0, 9) is True
assert recherche(9, list(range(10)), 0, 9) is True
assert recherche(10, list(range(10)), 0, 9) is False
assert recherche('a', list("abcdef"), 0, 5) is True
assert recherche('z', ["a", "b"], 0, 0) is False

