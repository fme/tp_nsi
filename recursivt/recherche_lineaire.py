def recherche_rec(seq, x) -> bool:
    """
    renvoie True si x esst dans seq, False autrement
    """
    if len(seq) == 0:
        return False
    return seq[0] == x or recherche_rec(seq[1:], x)


# EXERCICE : insérer des print pour suivre les appels récursifs

# un appel de recherche_rec ne « voit » que l'élément en tête de liste.

from random import randint
test = [randint (0, 100) for _ in range(100)]
for el in test:
    assert recherche_rec(test, el) is (el in test) 
assert recherche_rec(test, 101) is False



def recherche_rec_indice(seq, i, x) -> bool:
    """
    revoie True si x est dans seq à partir de l'indice départ inclus
    False dans le cas contraire
    """
    if i == len(seq):
        return False
    return seq[i] == x or recherche_rec_indice(seq, i + 1, x)



test = [randint (0, 100) for _ in range(100)]
for el in test:
    assert recherche_rec_indice(test, 0, el) is (el in test) 
assert recherche_rec_indice(test, 0, 101) is False


