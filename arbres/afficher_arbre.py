from arbre_binaire import Noeud, parcours_largeur, hauteur

"""
affichage d'arbre binaire sans prétention, mais pratique 
pour visualiser des cas simples.
Le dessin est défectueux si les étiquettes des nœuds font plus
d'un seul caractère.
"""

def afficher_arbre(racine: Noeud):
    haut = hauteur(racine)
    larg = (2**haut)*3
    pile = [(racine, 0, 0, larg, 0)]
    tab = [[" " for i in range(larg)] for j in range(2*haut+1)]
    tab[ 0 ][ larg//2 ] = str(racine.nom)
    while len(pile) > 0:
        noe, niv, pre, der, posp = pile.pop()
        mil = (pre + der) // 2
        if noe is not None and niv > 1:            # 
            tab[ niv ][ mil ] = str(noe.nom)
            c = 2 
            if mil < posp:                          # gauche
                tab[ niv-1 ][ mil+1 ] = "/"
                while mil + c < posp:
                    tab[ niv-2 ][ mil+c ] = "_"
                    c += 1
            else:
                tab[ niv-1 ][ mil-1] = "\\"
                while mil-c > posp:
                    tab[ niv-2 ][ mil-c ] = "_"
                    c += 1

        if noe.gauche is not None:
            pile.append((noe.gauche, niv+2, pre, mil-1, mil))
        if noe.droite is not None:
            pile.append((noe.droite, niv+2, mil+1, der, mil))

    for li in range(len(tab)):
        print("".join(tab[li]))
                

