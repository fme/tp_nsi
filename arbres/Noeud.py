# DS du 20-01-2025
class Noeud():
    def __init__(self, v):
        self.gauche = None
        self.droite = None
        self.valeur = v

    def insere(self, v):
        n = self
        est_insere = False
        while not est_insere :
            if v == n.valeur:           # bloc 1
                est_insere = True       # fin bloc 1
            elif v < n.valeur:          # bloc 2
                if n.gauche != None:
                    n = n.gauche
                else:
                    n.gauche = Noeud(v)
                    est_insere = True   # fin bloc 2
            else:                       # bloc 3
                if n.droite != None:
                    n = n.droite
                else:
                    n.droite = Noeud(v)
                    est_insere = True   # fin bloc 3


    def insere_tout(self, vals):
        for v in vals:
            self.insere(v)

    def recherche(self, v):
        """
        recherche la valeur v dans l'arbre ou le sous-arbre 
        enraciné en self.  question 4
        """
        if v < self.valeur and self.gauche is not None:
            return self.gauche.recherche(v)
        elif self.valeur < v and self.droite is not None:
            return self.droite.recherche(v)
        elif self.valeur == v:
            return True
        else:
            return False

    def recherche_ite(self, v):
        while self is not None:
            if v < self.valeur:
                self = self.gauche
            elif self.valeur < v:
                self = self.droite
            else:
                return True
        return False

    def affichage(self, niv = 0):
        """
        parcours en profondeur prefixe avec
        affichahe des valeurs des noeuds indenté
        selon la profondeur
        """
        print("  " * niv + str(self.valeur))
        if self.gauche is not None:
            self.gauche.affichage(niv+1)
        if self.droite is not None:
            self.droite.affichage(niv+1)

    def parcours_infixe(self):
        if self.gauche is not None:
            self.gauche.parcours_infixe()
        print(self.valeur, end=" ")
        if self.droite is not None:
            self.droite.parcours_infixe()


    def parcours_suffixe(self):
        """
        on peut aussi utiliser une autre syntaxe pour 
        les appels récursifs. 
        """
        if self is not None:
            Noeud.parcours_suffixe(self.gauche)
            Noeud.parcours_suffixe(self.droite)
            print(self.valeur, end=" ")


# tests et questions
if __name__ == "__main__":
    # arbre de la figure 1
    # question 3.2
    racine = Noeud(18)
    racine.insere_tout([15, 13, 16, 12, 23, 19, 21, 32])
    # affichage
    print("figure 1")
    racine.affichage()
    # question 2
    print("question 2")
    racine.parcours_infixe()
    print()
    racine.parcours_suffixe()
    print("question 3.1")
    racine = Noeud(18) 
    racine.insere_tout([12, 13, 15, 16, 19, 21, 32, 23]) 
    racine.affichage()
    # tests fonction recherche
    assert racine.recherche(13) is True
    assert racine.recherche(99) is False
    assert racine.recherche_ite(16) is True
    assert racine.recherche_ite(99) is False

