"""
représenter de la même manière l'arbre2

arbre1                    arbre2

           R                 R
         /   \             /   \
        A     B           A     B
      /  \    |         /  \ 
     C   D    E        C   D 
                          / \
                         E   F
"""

arbre1 = {'R':
          ({'A':
            ({'C': None}, {'D': None})},
           {'B':
            ({'E': None},)})}

def parcours_en_profondeur(arbre):
    nom, enfants = list(arbre.items())[0]
    print(nom)
    if enfants:
        for enfant in enfants:
            parcours_en_profondeur(enfant)

if __name__ == "__main__":
    parcours_en_profondeur(arbre1)




