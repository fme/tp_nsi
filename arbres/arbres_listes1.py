"""

arbre1                    arbre2

           R                 R
         /   \             /   \
        A     B           A     B
      /  \    |         /  \ 
     C   D    E        C    D
                          /  \
                         E    F
"""

arbre1 = ['R',
          [['A',
           ['C'],
           ['D']],
          ['B',
           ['E']]]]

def parcours_en_profondeur(arbre):
    print(arbre[0])
    if len(arbre) > 1:
        for e in arbre[1]:
            parcours_en_profondeur(e)


if __name__ == "__main__":
    parcours_en_profondeur(arbre1)




