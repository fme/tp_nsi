class Noeud:
    def __init__(self, nom, gauche=None, droite=None):
        self.nom = nom
        self.gauche = gauche
        self.droite = droite

    def __repr__(self):
        return str(self.nom)

# fonctions qui prennent comme argument la racine d'un AB

def parcours_profondeur_pref(racine: Noeud):
    if racine:
        print(racine)
        parcours_profondeur_pref(racine.gauche)
        parcours_profondeur_pref(racine.droite)


def parcours_profondeur_pref_iteratif(racine: Noeud):
    pile = [racine]
    while len(pile) > 0:
        noeud_courant = pile.pop()
        print(noeud_courant)
        if noeud_courant.droite:
            pile.append(noeud_courant.droite)
        if noeud_courant.gauche:
            pile.append(noeud_courant.gauche)
           

def parcours_largeur(racine: Noeud):
    file = [racine]
    while len(file) > 0:
        noeud_courant = file.pop(0)
        print(noeud_courant)
        if noeud_courant.gauche:
            file.append(noeud_courant.gauche)
        if noeud_courant.droite:
            file.append(noeud_courant.droite)
           


def taille(racine: Noeud) -> int:
    """
    renvoie le nombre de noeuds dans l'arbre enraciné en le Noeud racine
    """
    if racine:
        return taille(racine.gauche) + taille(racine.droite) + 1
    else:
        return 0

def hauteur(racine: Noeud) -> int:
    """
    renvoie la hauteur de l'arbre enraciné en le Noeud racine
    """
    if racine:
        return max(hauteur(racine.gauche), hauteur(racine.droite)) + 1
    else:
        return -1

# tests

if __name__ == "__main__":
    A = Noeud(10,
              Noeud(4,
                   Noeud(1),
                   Noeud(5)),
              Noeud(17,
                   Noeud(16),
                   Noeud(21)))
    B = Noeud('F',
              Noeud('B',
                   Noeud('A'),
                   Noeud('C')),
              Noeud('M',
                   Noeud('P'), None))

    # parcours
    parcours_profondeur_pref(A)
    parcours_profondeur_pref_iteratif(A)
    parcours_largeur(A)

    parcours_profondeur_pref(B)
    parcours_profondeur_pref_iteratif(B)
    parcours_largeur(B)

    assert taille(A) == 7
    assert taille(B) == 6

    assert hauteur(A) == hauteur(B) == 2






