# représentation d'arbre binaire sous forme de liste unique


gauche = lambda x: 2 * x
droite = lambda x: 2 * x + 1

def taille(arbre: list, i: int):
    """
    renvoie la taille de l'arbre binaire enraciné
    en le nœud d'index i
    """
    if i >= len(arbre) or arbre[i] is None:
        return 0
    else:
        return taille(arbre, gauche(i)) +\
                taille(arbre, droite(i)) + 1


def hauteur(arbre: list, i: int):
    """
    renvoie la hauteur de l'arbre binaire enraciné en
    le nœud d'indice i
    """
    if i >= len(arbre) or arbre[i] is None:
        return -1
    else:
        return max(hauteur(arbre, gauche(i)),
                   hauteur(arbre, droite(i))) + 1         

def parcours_en_profondeur_prefixe(arbre: list, i: int):
    """
    affiche les valeurs de l'arbre binaire enraciné en l'indice i
    dans l'ordre d'un parcours en profondeur préfixe
    """
    if i < len(arbre) and arbre[i] is not None:
        print(arbre[i])
        parcours_en_profondeur_prefixe(gauche(i))
        parcours_en_profondeur_prefixe(gauche(i))


# adaptation de la fonction afficher_arbre
def afficher_arbre(arbre: list, i:int):
    haut = hauteur(arbre, i)
    larg = (2**haut)*3
    pile = [(i, 0, 0, larg, 0)]
    tab = [[" " for i in range(larg)] for j in range(2*haut+1)]
    tab[ 0 ][ larg//2 ] = str(arbre[i])
    while len(pile) > 0:
        i, niv, pre, der, posp = pile.pop()
        mil = (pre + der) // 2
        if niv > 1:            # 
            tab[ niv ][ mil ] = str(arbre[i])
            c = 2 
            if mil < posp:                          # gauche
                tab[ niv-1 ][ mil+1 ] = "/"
                while mil + c < posp:
                    tab[ niv-2 ][ mil+c ] = "_"
                    c += 1
            else:
                tab[ niv-1 ][ mil-1] = "\\"
                while mil-c > posp:
                    tab[ niv-2 ][ mil-c ] = "_"
                    c += 1

        if gauche(i) < len(arbre) and arbre[gauche(i)] is not None:
            pile.append((gauche(i), niv+2, pre, mil-1, mil))
        if droite(i) < len(arbre) and arbre[droite(i)] is not None:
            pile.append((droite(i), niv+2, mil+1, der, mil))

    for li in range(len(tab)):
        print("".join(tab[li]))


if __name__ == "__main__":
    A  = [None, 'a', 'b', 'c', 'd', 'e', 'f', 'g']
    assert hauteur(A, 1) == 2
    assert taille(A, 1) == 7
    afficher_arbre(A, 1)
    print("=" * 30)
    A.extend([None] * 2 + ["h", "i"])
    assert taille(A, 1) == 9
    assert hauteur(A, 1) == 3

    afficher_arbre(A, 1)
