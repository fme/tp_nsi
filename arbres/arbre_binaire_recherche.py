from arbre_binaire import Noeud, parcours_profondeur_pref, hauteur, taille
from afficher_arbre import afficher_arbre

def recherche_ABR(racine: Noeud, x):
    """
    renvoie l'objet Noeud dont la valeur est x
    dans l'racine enraciné en racine, ou bien None si
    aucun noeud n'est porteur de x
    """
    while racine is not None and racine.nom != x:
        racine = racine.gauche if x < racine.nom else racine.droite
    return racine

# récursif
def recherche_ABR_rec(racine: Noeud, v):
    if racine is None or racine.nom == v:
        return racine
    elif v < racine.nom:
        return recherche_ABR_rec(racine.gauche, v)
    else:
        return recherche_ABR_rec(racine.droite, v)

def val_mini_ABR(racine: Noeud):
    while racine.gauche:
        racine = racine.gauche
    return racine.nom

# ou bien récursivement
def val_mini_ABR(racine: Noeud):
    if racine.gauche:
        return val_mini_abr(racine.gauche)
    else: return racine.nom


def ajouter_noeud_ABR(racine: Noeud, nouv: Noeud):
    """
    Insère un nouveau noeud dans l'abr dont la racine
    est racine. Si le nouveau noeud a un nom qui existe déjà
    dans l'ABR, la fonction lève une exception.
    """
    while racine is not None:
        parent = racine
        if nouv.nom == racine.nom:
            raise Exception("nom déjà présent")
        elif nouv.nom <= racine.nom:
            racine = racine.gauche
        else:
            racine = racine.droite
    if nouv.nom < parent.nom:
        parent.gauche = nouv
    else:
        parent.droite = nouv


# tests
if __name__ == "__main__":
    A = Noeud('M',
              Noeud('F',
                    Noeud('B', Noeud('A'), Noeud("C")),
                    None),
              Noeud('Q',
                   Noeud('P', Noeud('N'), None),
                   Noeud('T', Noeud("R"), None)))
    afficher_arbre(A)
    assert recherche_ABR(A, 'F') is not None 
    assert recherche_ABR_rec(A, 'F') is not None
    assert recherche_ABR(A, "Z") is None
    assert recherche_ABR_rec(A, "Z") is None
    
    ajouter_noeud_ABR(A, Noeud("G"))
    assert recherche_ABR(A, "G") is not None
    assert recherche_ABR_rec(A, "G") is not None
    afficher_arbre(A)




