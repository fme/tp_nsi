class Lapin:
    """
    un lapin capable de parthenogenèse
    """
    nombre_lapins = 0
    
    def __init__(self, nom:str=""):
        self.nom = nom
        Lapin.nombre_lapins += 1         # Lapin.nombre_lapins et non self.nombre_lapins
        self.bebe1 = None
        self.bebe2 = None
    
    def faire_un_bebe(self):
        if self.bebe1 is None:
            self.bebe1 = Lapin(self.nom + "1")
        elif self.bebe2 is None:
            self.bebe2 = Lapin(self.nom + "2")
        else:
            print("dejà deux bebes")
        

def agrandir_famille(parent, profondeur, profond_max):
    if profondeur < profond_max:
        parent.faire_un_bebe()
        agrandir_famille(parent.bebe1, profondeur+1, profond_max)
        parent.faire_un_bebe()
        agrandir_famille(parent.bebe2, profondeur+1, profond_max)

        
if __name__ == "__main__":
    lapinot = Lapin("lapinot")
    agrandir_famille(lapinot, 0, 3)
    assert Lapin.nombre_lapins == 15
