# 1. tri sélection

def tri_selection (liste):
    """
    Trie par sélection la liste
    """
    for i in range(len(liste)-1):
        # recherche du minimum après l'élément d'indice i
        i_du_mini = i
        for j in range(i+1, len(liste)):
            if liste[j] < liste[i_du_mini]:
                i_du_mini = j

        # échange du minimum trouvé avec l'élément d'indice i
        liste[i], liste[i_du_mini] = liste[i_du_mini], liste[i]

# tests
tri = tri_selection
liste = list("abracadabra")
tri(liste)
assert liste == ['a', 'a', 'a', 'a', 'a', 'b', 'b', 'c', 'd', 'r', 'r'], "test\
        1.1 (tri selection)"

# 2 tri sélextion inverse

def tri_selection_inverse (liste):
    """
    Trie par sélection la liste à l'envers
    """

    for i in range(len(liste)-1, 0, -1):
        # recherche du minimum après l'élément d'indice i
        i_du_mini = i
        for j in range(i-1, -1, -1):
            if liste[j] < liste[i_du_mini]:
                i_du_mini = j

        # échange du minimum trouvé avec l'élément d'indice i
        liste[i], liste[i_du_mini] = liste[i_du_mini], liste[i]

# tests 
tri = tri_selection_inverse
liste = list("abracadabra")
tri(liste)
assert liste == ['a', 'a', 'a', 'a', 'a', 'b', 'b', 'c', 'd', 'r', 'r'], "test\
        1.1 (tri selection)"
print(liste)


