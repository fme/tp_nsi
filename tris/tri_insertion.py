def tri_insertion(liste):
    """
    tri par insertion
    """
    for i in range(1, len(liste)):
        x = liste[i]
        j = i - 1
        # j va reculer jusqu'à l'indice précédent la place
        # où x doit être inséré
        while j >= 0  and liste[j] > x:
            liste[j+1] = liste[j]  # décalages
            j -= 1                 # recul j
        liste[j+1] = x             # insertion x


def tri_insertion_decale(liste):
    """
    tri par insertion
    """
    for i in range(1, len(liste)):
        x = liste[i]
        j = i 
        # j va reculer jusqu'à l'indice précédent la place
        # où x doit être inséré
        while j >= 0  and liste[j-1] > x:
            liste[j] = liste[j-1]  # décalages
            j -= 1                 # recul j
            liste[j] = x             # insertion x
            print(liste)


def tri_insertion_sans_test(liste):
    """
    tri par insertion sans test qui empêche j de reculer trop
    """
    for i in range(1, len(liste)):
        x = liste[i]
        j = i - 1
        while liste[j] > x:
            liste[j+1] = liste[j]  # décalages
            j -= 1                 # recul j
            liste[j+1] = x             # insertion x
            print(j)


def tri_insertion_a_lenvers(liste):
    """
    tri par insertion à l'envers
    """
    for i in range(len(liste)-2, -1, -1):
        x = liste[i]
        j = i + 1
        while j < len(liste) and liste[j] < x:
            liste[j-1] = liste[j]
            j += 1
        liste[j-1] = x


