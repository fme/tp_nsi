
def tri_bulles(liste):
    """
    tri à bulles
    """
    for i in range(0, len(liste)-1):
        for j in range(len(liste)-1, i, -1):
            if liste[j] < liste[j-1]:
                liste[j-1], liste[j] = liste[j], liste[j-1]
        print(liste)
