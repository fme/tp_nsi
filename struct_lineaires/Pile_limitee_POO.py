class Pile_tableau():
    """
    structure de donnée de type dernier-entré-premier-sorti basée
    sur une liste python de longueur fixe.
    """
    
    def __init__(self, capacite: int):
        self.liste = [None] * capacite
        self.psommet = -1
        self.capacite = capacite
        
    def est_vide(self):
        return self.psommet == -1
    
    def est_pleine(self):
        return self.psommet == self.capacite - 1
        
    def empiler(self, val):
        if not self.est_pleine():
            self.psommet += 1
            self.liste[ self.psommet ] = val
        else:
            raise Exception("Pile pleine")
            
    def depiler(self):
        if not self.est_vide():
            x = self.liste[ self.psommet ]
            self.psommet -= 1
            return x
        else:
            raise Exception("Pile vide")

    def voir_sommet(self):
        return self.liste[ self.psommet ]
    


# tests
if __name__ == '__main__':
    p = Pile_tableau(7)

    for lettre in "abcd":
        p.empiler(lettre)

    assert p.depiler() == "d"
    assert p.liste == ['a', 'b', 'c', 'd', None, None, None]
    assert p.voir_sommet() == "c"
    assert p.depiler() == "c"

    p.empiler("y")
    p.empiler("z")

    assert p.depiler() == "z"

    while not p.est_vide():
        p.depiler()

    assert p.est_vide() and p.psommet == -1

