################################################################################
#    +------------------------------------------------+                        #
#    |  ...  X ... <éléments dans la file> ... Y ...  |                        #
#    +------------------------------------------------+                        #
#            ^                                 ^                               #
#          iprem -->                         ientr -->                         #
#    iprem et ientr ne cessent d'augmenter et ne sont pas forcément des        #
#    indices valables du tableau. Le rôle du % lors des entrée et sorties      #
#    est de calculer des indices corrects à partir de iprem et ientr           #
################################################################################


class File_tableau():
    """
    Structure de données de type premier entré premier sorti 
    implémentée à l'aide d'une liste dont on ne change jamais
    la taille.
    est_pleine : renvoie True si la structure est pleine, False autrement
    est_vide :  renvoie True si la structure est vide, False autrement
    entrer (objet) : ajoute un objet à la structure
    sortir : retire et renvoie l'objet qui est entré depuis le plus longtemps
    parmi ceux encore présents dans la structure
    """
    def __init__(self, capa: int):
        self.iprem = None # index du premier (à être entré, à sortir)
        self.ientr = 0    # index du prochain entré (place libre)
        self.tabl = [None] * capa
        
    def est_pleine(self):
        return self.ientr - self.iprem == len(self.tabl)
    
    def est_vide(self):
        return self.iprem is None or self.iprem == self.ientr
    
    def entrer(self, nouv):
        if self.iprem is None:
            self.iprem = 0
        if self.est_pleine():
            raise Exception("File pleine")
        self.tabl[self.ientr % len(self.tabl)] = nouv
        self.ientr += 1
                
    def sortir(self):
        if self.est_vide():
            raise Exception("Fifo vide")
        x = self.tabl[self.iprem % len(self.tabl)]
        self.iprem += 1
        return x

# tests
if __name__ == '__main__':
    f = File_tableau(5) 
    for i in "abcde":
        f.entrer(i)
    # a b c d e 
    assert f.est_pleine()

    for _ in range(3):
        f.sortir()
    # _ _ _ d e

    assert f.sortir() == "d"
    # _ _ _ _ e : 4 places libres

    for nouveau in ("f", "g", "h", "i", "j"): # 1 de trop
        try:
            f.entrer(nouveau)
        except Exception as err:
            assert nouveau == "j"
    # f g h i e

    assert f.sortir() == "e"
    assert f.sortir() == "f"
    # _ g h i _

    for _ in range(4): # 1 de trop
        try:
            f.sortir()
        except Exception as err:
            continue

    assert f.est_vide()
