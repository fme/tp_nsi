"""
classe Cellule et fonctions utiles pour traiter une liste chaînée
Même si une liste chaînée EST une cellule (la cellule de tête), on 
préfère ne pas définir ces fonctions comme méthodes de la classe
Cellule mais comme des fonctions qui prennent comme argument une 
Cellule, considérée comme la tête de liste.
"""

class Cellule:
    def __init__(self, info, suiv=None):
        self.info = info
        self.suiv = suiv

    def __repr__(self):
        return str(self.info)

def creer_lc_depuis_sequence(valeurs):
    """
    renvoie la première cellule d'une liste chaînée contenant
    les valeurs qui sont dans la sequence (liste, tuple, str)
    valeurs, dans le même ordre
    """
    if len(valeurs) > 0:
        return Cellule(valeurs[0], creer_lc_depuis_sequence(valeurs[1:]))


def parcourir_print(tete: Cellule):
    if tete:
        print(tete, end="->")
        parcourir_print(tete.suiv)

def inserer_apres(prec: Cellule, ajout: Cellule):
    assert prec is not None
    ajout.suiv = prec.suiv
    prec.suiv = ajout

def ajouter_a_la_fin(tete: Cellule, ajout: Cellule):
    assert tete is not None
    while tete.suiv is not None:
        tete = tete.suiv
    inserer_apres(tete, ajout)

def retirer(prec: Cellule):
    assert prec is not None and prec.suiv is not None
    prec.suiv = prec.suiv.suiv

def chercher(tete: Cellule, infocherchee):
    while tete is not None:
        if tete.info == infocherchee:
            return tete
        tete = tete.suiv
    

#
if __name__ == "__main__":

    # utile pour les tests
    def comparer_lc(liste_c: Cellule, valeurs):
        """
        renvoie true si la liste chaînée liste_c
        contient les mêmes valeurs dans le même
        ordre que la sequence valeurs
        """
        for v in valeurs:
            if liste_c and liste_c.info == v:
                liste_c = liste_c.suiv
            else:
                return False
        return liste_c is None

    tete = creer_lc_depuis_sequence("abcdef")
    prec_retirer = chercher(tete, "c")
    retirer(prec_retirer)
    assert comparer_lc(tete, "abcef")
    ajouter_a_la_fin(tete, Cellule("g"))
    assert comparer_lc(tete, "abcefg")
    preced = chercher(tete, "e")
    inserer_apres(preced, Cellule("z"))
    assert comparer_lc(tete, "abcezfg")











