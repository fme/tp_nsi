# ds Tnsi du 16-01-2024 (18-12-2024): réaliser un pile avec deux files
# il fallait écrire la méthode dépiler

class File():
    """File basée sur une liste dynamique python."""
    
    def __init__(self):
        self.tab = []
    
    def est_vide(self):
        return len(self.tab) == 0
    
    def entrer(self, val):
        self.tab.append(val)
        
    def sortir(self):
        if self.est_vide():
            raise Exception ("File vide")
        return self.tab.pop(0)
    
    
class Pile_deux_files:
    """
    Implémentation d'une pile (structure LIFO)
    à l'aide de deux files (structures FIFO)
    """
    def __init__(self):
        self.file_A = File()
        self.file_B = File()
        
    def empiler(self, x):
        self.file_A.entrer(x)
        
    def depiler(self):
        """
        On va tranférer les éléments de self.file_A, la file 
        d'entrée, dans self.file_B, sauf le dernier. À la
        fin de la boucle, x est le dernier élément à être 
        entré dans self.file_A donc le premier à devoir 
        sortir dans la règle LIFO. C'est donc l'élément qui
        sera renvoyé. On inverse les rôles entre les deux
        files pour le prochain appel de la méthode. Le temps
        d'exécution est linéaire en le nombre d'éléments présents
        dans la pile (donc dans self.file_A).
        """
        if self.est_vide():
            raise Exception("Pile vide")

        while True:     # structure do ... while
            x = self.file_A.sortir()
            if self.file_A.est_vide():
                break
            self.file_B.entrer(x)
        self.file_A, self.file_B = self.file_B, self.file_A
        return x
        
    def est_vide(self):
        return self.file_A.est_vide()
    
# quelques tests et démonstration du fonctionnnement
if __name__ == "__main__":
    P = Pile_deux_files()
    for x in "abcde":
        print("Empile {}".format(x))
        P.empiler(x)
    while True:
        c = P.depiler()
        print("Depile {}".format(c))
        if c == 'c':
            break
    for x in "1234":
        print("Empile {}".format(x))
        P.empiler(x)
    while not P.est_vide():
        print("Dépile {}".format(P.depiler()))
    try:
        P.depiler() # -> erreur
    except Exception as e:
        assert e.args == ("Pile vide",)
