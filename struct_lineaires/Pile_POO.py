"""
Pile à base de liste python
"""

class Pile_liste():
    """
    Pile à base de liste
    méthodes : 
    empiler (objet)  ajoute un objet à la structure
    depiler ()       retire l'objet le plus recemment entré dans
    la structure et renvoie sa valeur
    est_vide ()      renvoie True si la structure est vide, False
    dans le cas contraire
    voir_sommet ()   renvoie la valeur de l'objet le plus recemment
    entré dans la structure sans le retirer
    """
    def __init__(self):
        self.liste = []
    
    def empiler(self, val):
        self.liste.append(val)
        
    def depiler(self):
        if len(self.liste) == 0:
            raise 
        Exception("même les super-héros ne peuvent dépiler une pile vide")
        return self.liste.pop()
    
    def est_vide(self):
        return len(self.liste) == 0

    def voir_sommet(self):
        return self.liste[ -1 ]

    def __repr__(self):
        return str(self.liste)

