from Pile_POO import Pile_liste as Pile

class File_2_piles():
    """
    Implémentation du TDA File à l'aide de 2 piles.
    1 pile pour les entrées, 1 pile pour les sorties.
    Les piles sont considérées illimitées, donc il n'y 
    aucune gestion des cas de pile pleine.
    """
    def __init__(self):
        self.entrees = Pile()
        self.sorties = Pile()
    
    def est_vide(self):
        return self.sorties.est_vide() and self.entrees.est_vide()
    
    def entrer(self, objet):
        self.entrees.empiler(objet) ### 1
    

    def sortir(self):
        if self.sorties.est_vide():
            if self.entrees.est_vide():
                raise Exception("File vide")
            else:
                while not self.entrees.est_vide():
                    self.sorties.empiler(self.entrees.depiler())
        return self.sorties.depiler()



# tests et démo
if __name__ == '__main__':
    f = File_2_piles()
    for i in range(7):
        f.entrer(i)

    print("entrées : ", f.entrees)
    print("sorties : ", f.sorties)
         
    assert f.sortir() == 0
    assert f.sortir() == 1
    assert f.sortir() == 2

    f.entrer(99)

    print("après 3 sorties et entrée de 99 :")
    print("entrées : ", f.entrees)
    print("sorties : ", f.sorties)

    for _ in range(4):
        f.sortir()

    assert f.sortir() == 99

    assert f.est_vide()

