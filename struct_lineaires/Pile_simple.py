"""
1ère implémentation d'une pile
* à base de list python
* avec les méthodes pop et append
* sans limite de taille
"""

def creer_pile_vide():
    return []

def est_vide(pile):
    return len(pile) == 0

def empiler(pile, x):
    pile.append(x)

def sommet(pile):
    if not est_vide(pile):
        return pile[-1]
    else:
        raise Exception("Pas de sommet d'une pile vide")

def depiler(pile):
    if not est_vide(pile):
        return pile.pop()
    else:
        raise Exception("même les super-héros ne peuvent dépiler une pile vide")


# tests
if __name__ == "main":
    pile_test = creer_pile_vide()
    for i in "abcdef":
        empiler(pile_test, i)
    assert sommet(pile_test) == "f"
    assert depiler(pile_test) == "f"
    assert depiler(pile_test) == "e"
    assert depiler(pile_test) == "d"
    assert depiler(pile_test) == "c"
    assert depiler(pile_test) == "b"
    assert sommet(pile_test) == "a"
    assert depiler(pile_test) == "a"


"""
TP
fonction nombre_elements
fonction dupliquer_pile
fonction nieme_dernier_entre
"""
def nombre_elements(pile):
    nbre = 0
    transit = []
    while not est_vide(pile):
        empiler(transit, depiler(pile))
        nbre += 1
    while not est_vide(transit):
        empiler(pile, depiler(transit))
    return nbre

# tests
if __name__ == "main":
    assert nombre_elements([1, 2, 3]) == 3
    assert nombre_elements([]) == 0


def dupliquer_pile(pile):
    transit = []
    copie = []
    
    while not est_vide(pile):
        empiler(transit, depiler(pile))
    
    while not est_vide(transit):
        a_empiler = depiler(transit)
        empiler(copie, a_empiler)
        empiler(pile, a_empiler)
        
    return copie

# tests
if __name__ == "main":
    pile_test = [1, 2, 3, 4]
    copie = dupliquer_pile(pile_test)
    assert copie == [1, 2, 3, 4]
    assert pile_test == [1, 2, 3, 4]
    assert copie is not pile_test
    assert dupliquer_pile([]) == []

def nieme_dernier(pile, n):
    """
    renvoie le nième dernier entré ou None 
    si n > nombre d'éléments dans la pile
    """

    transit = []
    renvoyer = None
    for _ in range(n):
        if not est_vide(pile):
            renvoyer = depiler(pile)
            empiler(transit, renvoyer)
        else:
            renvoyer = None
    
    while not est_vide(transit):
        empiler(pile, depiler(transit))
    return renvoyer

# tests
if __name__ == "main":
    pile_test = []
    for lettre in "abcdefgh":
        empiler(pile_test, lettre)
    assert nieme_dernier(pile_test, 1) == 'h'
    assert nieme_dernier(pile_test, 2) == "g"
    assert nieme_dernier(pile_test, 3) == "f"
    assert nieme_dernier([], 0) is None
    assert nieme_dernier([1, 2, 3], 0) is None
    assert nieme_dernier([1, 2, 3], 9) is None
