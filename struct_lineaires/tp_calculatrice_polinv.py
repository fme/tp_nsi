from Pile_POO import Pile_liste

"""
calculatrice utilisant une pile pour stoker les valeurs et les résulats
intermédiaires, calquée sur la manière dont python évalue les expressions
arithmétiques. Les expressions arithmétiques sont en polonaise inversée.

1ère version : accepte les expressions avec des entiers positifs sur 1 chiffre
décimal.
2ème version : accepte les expressions avec des variables.

faire par ex : 

    <code python>
    def f(a, b, c, d):
        return (a + b) * c - a * d

    import dis
    dis.dis(f)
    </code python>

pour constater que l'interpréteur cpython ne procède pas autrement
voir pour explication des opéations LOAD_FAST et BINARY_OP :
https://docs.python.org/3/library/dis.html#python-bytecode-instructions

  1           0 RESUME                   0

  2           2 LOAD_FAST                0 (a)
              4 LOAD_FAST                1 (b)
              6 BINARY_OP                0 (+)
             10 LOAD_FAST                2 (c)
             12 BINARY_OP                5 (*)
             16 LOAD_FAST                0 (a)
             18 LOAD_FAST                3 (d)
             20 BINARY_OP                5 (*)
             24 BINARY_OP               10 (-)
             28 RETURN_VALUE

"""

def operation(a, op, b):
    """Calcule une expression sans parenthèses"""
    a, b = float(a), float(b)
    if op == "+":
        res =  a + b
    elif op == "-":
        res = a - b
    elif op == "*":
        res = a * b
    elif op == "/":
        res = a / b
    else:
        raise Exception("Opérateur inconnu : -{}-".format(c))
    return res


def calculatrice(expr: str) -> float:
    """
    """
    pile = Pile_liste()
    for tok in expr:
        if tok.isdigit():
            pile.empiler(int(tok))
        if tok in "+-*/":
            droite = pile.depiler()
            gauche = pile.depiler()
            pile.empiler(operation(gauche, tok, droite))
    return pile.depiler()
    

# tests
if __name__ == "main":
    # (3 + 7) * 2 + 4 * 9
    test1 = "37+2*49*+"
    assert calculatrice(test1) == (3 + 7) * 2 + 4 * 9
    # 7 * (1 + 3) / ((3 + 2) * (1 - 7))
    test2 = "713+*32+17-*/"
    assert calculatrice(test2) == 7 * (1 + 3) / ((3 + 2) * (1 - 7))

"""
version qui accepte les expressions avec des variables
"""

def calculatrice_var(expr: str, noms: dict) -> float:
    pile = Pile_liste()
    for tok in expr:
        if tok in noms:
            pile.empiler(noms[ tok ])
        if tok in "+-*/":
            droite = pile.depiler()
            gauche = pile.depiler()
            pile.empiler(operation(gauche, tok, droite))
    return pile.depiler()

if __name__ == "main":
    # (a + b) * c - a * d
    test1 = "ab+c*ad*-"
    a, b, c, d = 1, 2, 3, 4
    noms = {nom: val for (nom, val) in zip("abcd", [1, 2, 3, 4])}
    assert calculatrice_var(test1, noms) ==  (a + b) * c - a * d

