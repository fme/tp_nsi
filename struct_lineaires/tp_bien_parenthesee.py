from Pile_POO import Pile_liste

"""
https://codex.forge.apps.education.fr/exercices/expression_bien_parenthesee_2/
"""

ouvrant = "([{<"
fermant = ")]}>"
ouverture = {  # dictionnaire qui donne l'ouvrant en fonction du fermant
    ")": "(",
    "]": "[",
    "}": "{",
    ">": "<",
}


def est_bien_parenthesee(expression):
    pile = Pile_liste()
    for c in expression:
        if c in ouvrant:
            pile.empiler(c)
        if c in fermant:
            if pile.est_vide() or pile.depiler() != ouverture[ c ]:
                return False
    return pile.est_vide()


# Tests

if __name__ == "main":
    assert est_bien_parenthesee("(2 + 4)*7") is True
    assert est_bien_parenthesee("tableau[f(i) - g(i)]") is True
    assert (
        est_bien_parenthesee(
            "int main(){int liste[2] = {4, 2}; return (10*liste[0] + liste[1]);}"
        )
        is True
    )

    assert est_bien_parenthesee("(une parenthèse laissée ouverte") is False
    assert est_bien_parenthesee("{<(}>)") is False
    assert est_bien_parenthesee("c'est trop tard ;-)") is False
