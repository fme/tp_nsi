"""
implémentation d'une pile de taille fixée. 
à comparer avec la version POO

la variable psommet est l'index du psommet de la pile. 
"""

psommet = -1

def creer_pile(capacite: int):
    # global psommet
    psommet = -1
    return [None] * capacite

def est_vide(pile):
    return psommet == -1

def est_pleine(pile):
    return psommet == len(pile) - 1

def empiler(pile, val):
    global psommet
    if not est_pleine(pile):
        psommet += 1
        pile[ psommet ] = val
    else:
        raise Exception("Pile pleine")

def depiler(pile):
    global psommet
    if not est_vide(pile):
        x = pile[ psommet ]
        psommet -= 1
        return x
    else:
        raise Exception("Pile vide")

def voir_sommet(pile):
    return pile[ psommet ]



# tests
if __name__ == "main":

    p = creer_pile(7)
    for lettre in "abcd":
        empiler(p, lettre)

    assert depiler(p) == "d"
    assert p == ['a', 'b', 'c', 'd', None, None, None]
    assert voir_sommet(p) == "c"

    while not est_vide(p):
        depiler(p)

    assert est_vide(p) and psommet == -1
