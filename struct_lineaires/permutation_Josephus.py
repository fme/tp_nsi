#!/usr/bin/env python
# coding: utf-8

# # Permutations de Josephus
# 
# Le problème est nommé d'après Flavius Josèphe, un historien du 1er siècle. Selon le récit de Josèphe sur le siège de Yodfat par les romains, lui et ses 40 soldats ont été piégés dans une grotte par des soldats romains. Ils ont choisi le suicide plutôt que la capture et ont opté pour une méthode de suicide en série par tirage au sort. Josèphe affirme que par chance ou peut-être par la main de Dieu, lui et un autre homme sont restés jusqu'à la fin et se sont rendus aux Romains plutôt que de se suicider. C'est l'histoire donnée dans le livre 3, chapitre 8, partie 7.

class Cellule():
    def __init__(self, valeur, suivante=None):
        self.val = valeur
        self.suiv = suivante
        
def cercle(n: int) -> Cellule:
    """
    Renvoie la première cellule d'un cercle 
    de Josephus avec n personnes
    
    n ne doit pas être inférieur ou égal à zéro.
    """
    tete = dern = Cellule(n)
    for i in range(n-1, 0, -1):
        tete = Cellule(i, tete)
    dern.suiv = tete
    return tete
   

def nieme(prem: Cellule, n: int) -> Cellule:
    """
    Renvoie la n-ième cellule d'une liste 
    chaînée, en partant de prem comme n° 1
    
    n ne doit donc pas être inférieur ou égal à zéro.
    """
    while prem is not None and n > 1:
        prem = prem.suiv
        n = n - 1
    return prem


def eliminer(cell: Cellule) -> Cellule:
    """
    Supprime la cellule cell dans une liste chaînée
    circulaire et renvoie sa suivante.
    """
    prec = cell
    while prec.suiv != cell:
        prec = prec.suiv
    prec.suiv = cell.suiv
    return prec.suiv


def josephus(prem: Cellule, m: int) -> str:
    """
    Renvoie la suite des nombres d'une permutation
    de Josephus en partant de la personne n° m
    
    m ne doit donc pas être inférieur ou égal à zéro.
    """
    permut = ""
    while prem.suiv != prem:
        e = nieme(prem, m)
        permut += str(e.val)
        prem = eliminer(e)
        
    permut += str(prem.val)
    return permut


# tests cercle
if __name__ == "__main__":
    # test 1
    isinstance(cercle(1), Cellule)
    # test 2
    c = cercle(1)
    assert c.suiv == c
    assert c.val == 1
    # test 3
    c = cercle(2)
    assert c.suiv.suiv == c
    assert c.suiv.val == c.val + 1
    # test 4
    c = cercle(3)
    assert c.val == 1
    assert c.suiv.val == 2
    assert c.suiv.suiv.val == 3
    assert c.suiv.suiv.suiv.val == 1
    # test nieme
    c = Cellule(7)
    for i in range(6, 0, -1):
        c = Cellule(i, c)
    assert nieme(c, 1).val == 1
    assert nieme(c, 2).val == 2
    assert nieme(c, 3).val == 3
    assert nieme(c, 7).val == 7
    assert nieme(c, 8) is None
    # tests éliminer
    p = d = Cellule(7)
    for i in range(6, 0, -1):
        p = Cellule(i, p)
    d.suiv = p
    assert eliminer(p).val == 2
    # tests josephus
    c = cercle(7)
    assert josephus(c, 3) == "3627514"




