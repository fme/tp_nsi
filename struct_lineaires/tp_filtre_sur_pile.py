from Pile_limitee_POO import Pile_tableau

def filtre_pos(une_pile):
    """
    filtre_pos prend comme argument un objet une_pile, de la classe 
    Pile_tableau,  et renvoie une pile de la classe Pile_tableau dans lequel 
    se trouvent tous et seulement les entiers positifs qui sont dans une_pile, 
    dans le même ordre
    """
    pile_A = Pile_tableau(une_pile.capacite)
    pile_B = Pile_tableau(une_pile.capacite)

    while not une_pile.est_vide():
        x = une_pile.depiler()
        pile_A.empiler(x)
        if x >= 0:
            pile_B.empiler(x)
    
    while not pile_A.est_vide():
        une_pile.empiler(pile_A.depiler())

    while not pile_B.est_vide():
        pile_A.empiler(pile_B.depiler())

    return pile_A


# test
ptest = Pile_tableau(8)
for i in (-1, 2, -1, 3, -1, 4, -1, 5):
    ptest.empiler(i)

res = filtre_pos(ptest)

assert res.liste[ :res.psommet+1 ] == [2, 3, 4, 5]

