"""
Classe Liste sous forme de liste chaînée (simplement chaînée).
L'objet Liste ne contient qu'une référence à la tête de liste, qui 
est None à la création (liste vide) et qui est la première cellule
ensuite.
Utile pour implémenter une pile ou une file.
"""


class Cellule():
    """Maillon d'une liste chaînée simple"""
    
    def __init__(self, info, suivante=None):
        self.info = info 
        self.suiv = suivante
        
    def __repr__(self):
        return str(self.info)



class Liste:
    def __init__(self):
        self.tete = None

    def est_vide(self):
        return self.tete is None

    def ajouter_a_la_fin(self, ajout: Cellule):     # append
        """
        ajout doit être un objet de la classe Cellule
        """
        if not self.tete:
            self.tete = ajout
        else:
            t = self.tete
            while t.suiv:
                t = t.suiv
            t.suiv = ajout

    def ajouter_au_debut(self, ajout: Cellule):     # insert(0, _)
        """
        ajout doit être un objet de la classe Cellule
        """
        if self.tete:
            ajout.suiv = self.tete
        self.tete = ajout

    def supprimer_tete(self): # pop(0)
        if self.tete:
            x = self.tete.info
            self.tete = self.tete.suiv
            return x

    def __getitem__(self, nieme: int):
        """
        la cellule de tete a le numéro 0
        si tete est None, erreur
        """
        if self.tete:
            t = self.tete
            for _ in range(nieme):
                t = t.suiv
            return t
        else:
            raise ValueError()

if __name__== "__main__":
    x = Liste()
    for a in (2, 3, 4):
        x.ajouter_a_la_fin(Cellule(a))

    assert x.tete.info == 2
    assert x.tete.suiv.info == 3
    assert x.tete.suiv.suiv.info == 4
    assert x.tete.suiv.suiv.suiv is None

    x.ajouter_au_debut(Cellule(1))
    x.ajouter_au_debut(Cellule(0))

    assert x.tete.info == 0
    assert x.tete.suiv.info == 1
    assert x.tete.suiv.suiv.info == 2

    x.supprimer_tete() # 0
    assert x.tete.info == 1
    assert x.tete.suiv.info == 2
    assert x.tete.suiv.suiv.info == 3

    assert x[0].info == 1
    assert x[1].info == 2
    assert x[2].info == 3

    while not x.est_vide():
        x.supprimer_tete()


