def max_de_3(x, y, z):
    if x < y:
        if y < z:
            return z
        else:
            return y
    else:
        if x < z:
            return z
        else:
            return x

# tests
if __name__ == "__main__":
    # fuzzing
    from random import randint
    binf, bsup = -10, 10
    for _ in range(1000):
        x, y, z = randint(binf, bsup),randint(binf, bsup), randint(binf, bsup)
        assert max_de_3(x, y, z) == max(x, y, z), F"x={x}, y={y}, z={z}"




