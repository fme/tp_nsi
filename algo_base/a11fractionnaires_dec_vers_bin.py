##convertir un nombre* 0 <= d < 1 en binaire
def fractionnaire_en_binaire(d: float, nbbits: int) -> str:
    """
    renvoie le développement binaire du nombre d (la partie
    après la virgule de son écriture en base 2). d DOIT être
    compris entre 0 inclus et 1 exclu. 
    Si le développement binaire de d est infini, seuls les nbbits
    premiers chiffres sont calculés.
    Si le développement binaire de d compte moins de nbbits bits,
    il est complété à droite par des zéros.
    """
    assert 0 <= d < 1, "nombre hors intervalle"
    res = ""
    c=0
    while d > 0 and c < nbbits:
        res = res + str(int(2*d))
        d = 2 * d - int(2*d)
        c = c +1
    return res.ljust(nbbits, "0")

# tests
nbtest = 0.1
r1 = fractionnaire_en_binaire(nbtest, 32)
print("{} -> 0,{}".format(nbtest, r1))
print("erreur = {}".format(nbtest - int(r1, 2) / 2**len(r1)))

nbtest = 0.7
r1 = fractionnaire_en_binaire(nbtest, 10)
print("{} -> 0,{}".format(nbtest, r1))
print("erreur = {}".format(nbtest - int(r1, 2) / 2**len(r1)))

nbtest = 0.125
r1 = fractionnaire_en_binaire(nbtest, 10)
print("{} -> 0,{}".format(nbtest, r1))
print("erreur = {}".format(nbtest - int(r1, 2) / 2**len(r1)))
