# conversion d'un nombre d'une écriture décimale à binaire

def dec_vers_bin(dec: int) -> str:
    """
    entrée : un nombre entier positif dec
    sortie : sa représentation binaire, sous forme de str
    """
    res = ""
    if dec == 0:
        return "0"
    while dec > 0:        
        res = str(dec % 2) + res
        dec = dec // 2
    return res    

# tests
nbtests = 10
from random import randint
for i in range(nbtests):
    test = randint(0, 10000)
    assert dec_vers_bin(test) == bin(test)[ 2: ], F"test {i} : {test}"


# sur un nombre donné de caractères
def dec_vers_bin_format(dec: int, nbchiffres: int) -> str:
    """
    entrée : un nombre entier positif dec
    sortie : sa représentation binaire, sous forme de str, sur un 
    nombre donné nbchiffres de caractères
    """
    copiedec = dec
    res = ["0"] * nbchiffres
    i = 1
    while i < nbchiffres and dec > 0:
        res[ -i ] = str(dec % 2)
        dec = dec // 2
        i += 1
    assert i < nbchiffres, F"pas assez de chiffres de format pour {copiedec}"
    return "".join(res)


# tests
assert dec_vers_bin_format(0, 8) == "00000000"
assert dec_vers_bin_format(53, 8) ==  "00110101"
assert dec_vers_bin_format(53, 16) ==  "0000000000110101"


# hexadécimal, avec format

def dec_vers_hex(dec: int, nbchiffres: int) -> str:
    """
    Renvoie une str qui est la représentation hexadécimale du nombre
    entier positif dec, sur nbchiffres chiffres 
    """
    d2h = "0123456789abcdef"
    hexa = ""     
    while dec != 0 :
        hexa = d2h[ dec % 16 ] + hexa
        dec = dec // 16
    return "0" * (nbchiffres - len(hexa)) + hexa

# tests
assert (dec_vers_hex(53, 8)) == "00000035"
assert (dec_vers_hex(3405707998, 8)) == "cafefade" 




