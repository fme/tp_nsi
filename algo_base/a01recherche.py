def recherche_lineaire(seq, x) -> bool:
    """
    renvoie true si x est dans seq, False dans le cas contraire
    """
    for i in range(len(seq)):
        if seq[i] == x:
            return True
    return False

def recherche_lineaire_indice(seq, x) -> int:
    """
    renvoie l'indice la première occurrence de x dans seq
    ou None si x n'est pas dans seq
    """
    for i in range(len(seq)):
        if seq[i] == x:
            return i
    return None

def recherche_lineaire_dern_indice(seq, x) -> int:
    """
    renvoie l'indice de la dernière occurrence de x dans seq, ou bien None si x
    n'est pas dans seq
    """
    for i in range(len(seq)-1, -1, -1):
        if seq[i] == x:
            return i
    return None

from random import randint
test = [randint (0, 100) for _ in range(100)]
for el in test:
    assert recherche_lineaire(test, el) is (el in test) 

test = list(range(100))
assert recherche_lineaire_indice(test, 42) == 42
    


