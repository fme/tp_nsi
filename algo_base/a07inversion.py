# inversion d'une liste, inversion d'une str

def inverser_liste(liste: list):
    """
    Inverse l'ordre des éléments dans la liste
    Ne renvoie rien.
    """
    i = 0
    j = len(liste) - 1
    while i < j:
        liste[i], liste[j] = liste[j], liste[i]
        i += 1
        j -= 1


# tests
inverser = inverser_liste
L = [1, 2, 3]
inverser(L)
assert L == [3, 2, 1]
L = []
inverser(L)
assert L == []
L = list("abracadabra")
inverser(L)
assert L == ['a', 'r', 'b', 'a', 'd', 'a', 'c', 'a', 'r', 'b', 'a']

# str : immutable, donc obligation de créer une nouvelle str

# v1 : en passant par une liste
def inverser_str_v1(s: str) -> str:
    """
    Renvoie une str composée des caractères de s dans l'ordre inverse
    """
    ls  = list(s)
    inverser_liste(ls)
    return "".join(ls)


assert inverser_str_v1("nsi") == "isn"
assert inverser_str_v1("abcdefgh") == "hgfedcba"

# v2 : avec concaténations
def inverser_str_v2(s: str):
    """
    Renvoie une str composée des caractères de s dans l'ordre inverse
    """
    r = ""
    for c in s:
        r = c + r
    return r

assert inverser_str_v2("nsi") == "isn"
assert inverser_str_v2("abcdefgh") == "hgfedcba"

# v3 : récursif 
def inverser_str_v3(s: str):
    """
    Renvoie une str composée des caractères de s dans l'ordre inverse
    """
    if s == "":
        return s
    return inverser_str_v3(s[1:]) + s[0]  


assert inverser_str_v3("nsi") == "isn"
assert inverser_str_v3("abcdefgh") == "hgfedcba"


