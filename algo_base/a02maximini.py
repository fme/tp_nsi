def maxi(seq) -> int:
    """
    renvoie l'élément max de la sequence seq
    """
    if len(seq) == 0:
        return None
    maxit = seq[0]
    for i in range(len(seq)):
        if seq[i] > maxit:
            maxit = seq[i]
    return maxit


def maxi_a_partir_de(seq, depart: int):
    """
    renvoie l'élément max de la sequence seq
    les éléments d'indice inférieur à depaart
    étant exclus de la recheche du max
    """
    if len(seq) - depart == 0:
        return None
    maxit = seq[depart]
    for i in range(depart, len(seq)):
        if seq[i] > maxit:
            maxit = seq[i]
    return maxit

# ou bien 

def maxi_a_partir_de_bis(seq, depart: int):
    return maxi(seq[depart:])

from random import randint
assert maxi([]) is None
assert maxi(list(range(10))) == 9
nb_tests = 100
for _ in range(nb_tests):
    test = [randint(0, 100) for _ in range(100)]
    assert maxi(test) == max(test)
assert maxi_a_partir_de([1, 2, 3, 4], 3) == 4
assert maxi_a_partir_de_bis([1, 2, 3, 4], 3) == 4
assert maxi_a_partir_de([1, 2, 3, 4], 4) is None




