def r_dicho (liste, v):
    """
    Renvoie True si v est dans la liste liste
    et False dans le cas contraire
    """
    pre = 0
    der = len(liste)-1
    while pre <= der:
        mil = (pre + der) // 2    
        if liste[mil] == v:
            return True
        elif v < liste[mil]:
            der = mil - 1
        else:
            pre = mil + 1
        
    return False
