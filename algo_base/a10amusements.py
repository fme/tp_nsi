"""
Quels sont les deux plus grands éléments de sq

    Entrée : une sq
    Sortie : les deux plus grands éléments de sq

Méthode 1 : avec une fonction annexe, inspirée du tri par sélection.
Méthode 2 : avec 2 variables. Attention aux valeurs initiales. 
Si le max de la liste est aussi le premier élément, si on fait 
max1 = max2 = sq[ 0 ] ligne 2, alors, max2 prend la valeur du max 
de la liste et le test l. 7 ne peut plus devenir vrai
"""

def premier_deuxieme_B(sq):
    max1 = max2 = -float('inf') 
    for e in sq:
        if e > max1:
            max2 = max1
            max1 = e
        elif max2 < e:
            max2 = e
    return max1, max2  


# exercice : modifier pour retourner les 2 plus petits.


def index_max_depuis(sq, p):
    """
    Retourne l'index du maximum dans sq à partir de p
    """
    i_max = p
    for i in range(p, len(sq)):
        if sq[ i ] > sq[ i_max ]:
            i_max = i
    return i_max

def premier_deuxieme(sq):
    for i in range(2):
        m = index_max_depuis(sq, i)
        sq[ m ], sq[ i ] = sq[ i ], sq[ m ]
    return sq[ 0 ], sq[ 1 ]
