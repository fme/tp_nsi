# Remplacer toutes les occurrences de  x par  y dans liste 

def remplace(x, y, liste: list):
    """
    Entrée : des valeurs x, y, une liste L
    Sortie : la fonction remplace toutes les occurrences 
    de la valeur x par la valeur y dans L et retourne le
    nombre de remplacements effectuées.
    """
    nbre_rempl = 0
    for i in range(len(liste)):
        if liste[ i ] == x:
            liste[ i ] = y
            nbre_rempl += 1
    return nbre_rempl

#tests 
remplace_test1 = list("abracadabra")
assert remplace('a', 'u', remplace_test1) == 5, "remplace test 1"
assert remplace_test1 == list("ubrucudubru")
assert remplace(1, 2, [0] * 10) == 0, "remplace test 2"
L = [7, 1, 3, 8, 2, 1, 7, 2, 1, 1]
assert remplace(1, 33, L) == 4
assert L == [7, 33, 3, 8, 2, 33, 7, 2, 33, 33]


"""
exercice variante 1 : au lieu de remplacer les éléments dans la liste d'origine,
créer une nouvelle liste avec les remplacements.
"""
def remplace_copie_v1(x, y, liste: list) -> list:
    copie = []
    for e in liste:
        if e == x:
            copie.append(y)
        else:
            copie.append(e)
    return copie

def remplace_copie_v2(x, y, liste: list) -> list:
    copie = liste[:]
    remplace(x, y, copie)
    return copie

# tests
remplace_copie = remplace_copie_v1

remplace_test1 = list("abracadabra")
assert remplace_copie('a', 'u', remplace_test1) == list("ubrucudubru")
assert remplace_test1 == list("abracadabra")

remplace_test2 = [0] * 10
assert remplace_copie(0, 99, remplace_test2) == [99] * 10
assert remplace_test2 == [0] * 10

assert remplace_copie(1, 2, remplace_test2) == remplace_test2

remplace_copie = remplace_copie_v2

remplace_test1 = list("abracadabra")
assert remplace_copie('a', 'u', remplace_test1) == list("ubrucudubru")
assert remplace_test1 == list("abracadabra")

remplace_test2 = [0] * 10
assert remplace_copie(0, 99, remplace_test2) == [99] * 10
assert remplace_test2 == [0] * 10

assert remplace_copie(1, 2, remplace_test2) == remplace_test2


"""
pourquoi le code suivant ne fonctionne-t-il pas ? 
"""
def remplace(x, y, liste: list):
    """
    Entrée : des valeurs x, y, une liste L
    Sortie : la fonction remplace toutes les occurrences 
    de la valeur x par la valeur y dans L et retourne le
    nombre de remplacements effectuées.
    ET ÇA NE FONCTIONNE PAS !
    """
    nbre_rempl = 0
    for v in liste:
        if v == x:
            v = y
            nbre_rempl += 1
    return nbre_rempl

