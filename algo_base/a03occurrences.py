# Combien d'occurrences de la valeur x dans la séquence seq ?

def nombre_occurrences_v1(x, seq):
    """
    Entrée : une séquence seq (liste, tuple, str) et une valeur x
    Sortie : nombre d'occurrences de x dans seq.
    """
    compt = 0
    for v in seq:
        if v == x:
            compt += 1
    return compt


# built-in
def nombre_occurrences_v2(x, seq):
    return seq.count(x)


# tests
nombre_occurrences = nombre_occurrences_v1
assert nombre_occurrences(1, (7, 1, 3, 8, 2, 1, 7, 2, 1, 1)) == 4,\
        "nombre_occurrences test 1"
assert nombre_occurrences(9, (7, 1, 3, 8, 2, 1, 7, 2, 1, 1)) == 0,\
        "nombre_occurrences test 2"
assert nombre_occurrences("a", "abracadabra") == 5, "nombre_occurrences test 3"

nombre_occurrences = nombre_occurrences_v2
assert nombre_occurrences(1, (7, 1, 3, 8, 2, 1, 7, 2, 1, 1)) == 4,\
        "nombre_occurrences test 1"
assert nombre_occurrences(9, (7, 1, 3, 8, 2, 1, 7, 2, 1, 1)) == 0,\
        "nombre_occurrences test 2"
assert nombre_occurrences("a", "abracadabra") == 5, "nombre_occurrences test 3"


# dictionnaire d'occurrences 

def dict_occurrences_v1(seq):
    """
    Entrée : une séquence seq
    Sortie : un dictionnaire dont les clés sont les éléments
    distincts de seq et les valeurs correspondantes leurs nombres
    d'occurrences dans seq.
    """
    dic = {}
    for v in seq:
        dic[ v ] = dic[ v ] + 1 if v in dic else 1
    return dic

# variante
def dict_occurrences_v2(seq):
    return {k: nombre_occurrences(k, seq) for k in seq}

# QUESTION : quelle variante a le meilleur temps d'exécution ? 

# tests
dict_occurrences = dict_occurrences_v1
assert dict_occurrences("abracadabra") == {'a': 5, 'b': 2, 'c': 1, 'd': 1,
                                          'r': 2}, "dict_occurrences test 1"
assert dict_occurrences([1] * 11) == {1: 11}, "dict_occurrences test 2"
assert dict_occurrences([]) == {}, "dict_occurrences test 3"

dict_occurrences = dict_occurrences_v2
assert dict_occurrences("abracadabra") == {'a': 5, 'b': 2, 'c': 1, 'd': 1,
                                          'r': 2}, "dict_occurrences test 1"
assert dict_occurrences([1] * 11) == {1: 11}, "dict_occurrences test 2"
assert dict_occurrences([]) == {}, "dict_occurrences test 3"







