def tri_selection (liste):
    """
    Trie (par sélection) la liste
    """
    
    for i in range(len(liste)-1):
        #recherche du minimum après l'élément d'indice i
        i_du_mini = i
        for j in range(i+1, len(liste)):
            if liste[j] < liste[i_du_mini]:
                i_du_mini = j
        
        # échange du minimum trouvé avec l'élément d'indice i
        liste[i], liste[i_du_mini] = liste[i_du_mini], liste[i] 


# tests
test1 = [0, 1, 2, 3, 4, 5, 0]
tri_selection(test1)
assert test1 == [0, 0, 1, 2, 3, 4, 5]

import random

for _ in range(10):
    test = [random.randint(0, 100) for _ in range(100)]
    tri_selection(test) 
    assert test == sorted(test)



def tri_insertion(liste):
    """
    Trie (par insertion) la liste
    """
    for i in range(1, len(liste)):
        x = liste[i]
        j = i - 1
        # j va reculer jusqu'à l'indice précédant celui
        # où x doit être inséré 
        while j >= 0  and liste[j] > x:
            liste[j+1] = liste[j]  # décalages
            j -= 1                 # recul j
        liste[j+1] = x             # insertion x   


# tests
test1 = [0, 1, 2, 3, 4, 5, 0]
tri_selection(test1)
assert test1 == [0, 0, 1, 2, 3, 4, 5]

for _ in range(10):
    test = [random.randint(0, 100) for _ in range(100)]
    tri_selection(test) 
    assert test == sorted(test)


