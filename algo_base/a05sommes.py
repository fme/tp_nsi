def somme_liste(L: list) -> int:
    """
    Entrée : une liste L de nombres
    Sortie : la somme des éléments de L
    """
    s = 0  # la somme provisoire
    for e in L:
        s = s + e
    return s

# built in

sum([1, 2, 3, 4])


assert somme_liste(list(range(10))) == 45
assert somme_liste([]) == 0

