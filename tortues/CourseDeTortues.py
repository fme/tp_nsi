# -*-coding: utf-8 -*-
# VERSION DU 24/09/2024

from turtle import *
from random import randint

ECART = 30 # écart entre 2 tortues sur la ligne de départ

#commençons par tracer la ligne d'arrivée avec la tortue sans nom
penup()
goto(200,200)
pendown()
pensize(4)
goto(200,-200)
write("  Arrivée", font=("SansSerif",12,"normal"))
hideturtle()

#créons 3 tortues
t1 = Turtle()
t2 = Turtle()
t3 = Turtle()

# et trois couleurs
couleurs = ["blue", "red", "green"]

# on les met dans une liste
mes_tortues = [t1, t2, t3]

# on les colorie
for i in range(len(mes_tortues)):
    mes_tortues[i].color(couleurs[i])


# on les met en place
for i in range(len(mes_tortues)):
    mes_tortues[i].penup()
    mes_tortues[i].goto((-200, ECART * (i -1)))
    mes_tortues[i].pendown()


#1,2,3 partez !
classement = []  # indices des tortues déjà arrivées

while len(classement) < len(mes_tortues): # tant qu'il y a moins de tortues
    # arrivées que de tortues dans la liste mes_tortues ...
    for i in range(len(mes_tortues)):     
        if i not in classement:           
            mes_tortues[i].fd(randint(0, 10))
            mes_tortues[i].seth(randint(-30, 30))
            if mes_tortues[i].xcor() > 200:
                classement.append(i)  
                # attention, j'ajoute non pas une tortue mais son n° dans
                # la liste mes_tortues

# indiquer le classement
print("Classement : ")
for i in range(3):
    print(F"{i+1} : Tortue {couleurs[classement[i]]}")

exit(0)
