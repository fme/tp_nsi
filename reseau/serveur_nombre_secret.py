#!//usr/bin/python3
# -*-coding:utf-8 -*

"""
SERVEUR NOMBRE SECRET
"""

import socket as so
import threading as th
from random import randint
import sys


LIM_INF, LIM_SUP = 1, 100
DBG = True 

def jouer_avec(so_cli: so.socket, adr_cli: tuple):
    """
    boucle de jeu avec un client identifié par adr_cli
    en utilisant la socket dédiée so_cli
    """
    DBG or print("On joue avec {}:{}".format(adr_cli[0], adr_cli[1]))
    fini = False
    nb_secret = randint(1, 100)
    nb_essais = 0
    DBG or print("nombre secret : {}".format(nb_secret))
    so_cli.send("je suis prêt\n".encode("utf-8"))
    while not fini:
        try:
            msg_cli = int(so_cli.recv(128).decode("utf-8"))
            nb_essais += 1
            if msg_cli == nb_secret:
                reponse = "gagné en {} essais !\n".format(nb_essais)
                fini = True
            elif LIM_INF <= msg_cli < nb_secret:
                reponse = "trop petit\n"
            elif nb_secret < msg_cli <= LIM_SUP:
                reponse = "trop grand\n"
            else:
                reponse = "ton hypothèse doit être entre {} et {} inclus\n".\
                        format(LIM_INF, LIM_SUP)
        except ConnectionResetError:
            print("{}:{} a quitté abruptement".format(adr_cli[0], adr_cli[1]))
            break
        except (TypeError, ValueError, UnicodeDecodeError) as exc:
            DBG or print(exc)
            reponse = "message invalide, recommencez svp\n"
        except Exception as exc:
            print(exc)
            break
        try:
            so_cli.send(reponse.encode("utf-8"))
        except BrokenPipeError:
            DBG or print("connection perdue avec {}:{}".format(adr_cli[0],
                                                               adr_cli[1]))
            fini = True
        # fin boucle jeu
    DBG or print("fermeture connexion avec {}:{}".format(adr_cli[0],
                                                         adr_cli[1]))
    so_cli.close()
    # fin fonction jouer_avec


################################################################################
# usage : serveur_nombre_secret.py <numéro de port souhaité>
# ou    : serveur_nombre_secret

if len(sys.argv) == 2:
    nm_port = int(sys.arg[1])
    DBG or print("on utilise le port {}".format(sys.argv[1]))
    # vérifications sur le num port ? 
    adr_srv = ("", nm_port)
else:
    adr_srv = ["", 0]         # INADDR_ANY + choix du port par le systeme

so_srv = so.socket(so.AF_INET, so.SOCK_STREAM)
so_srv.setsockopt(so.SOL_SOCKET, so.SO_REUSEADDR, 1)
so_srv.bind(tuple(adr_srv))
nb_conn = 0
adr_srv = so_srv.getsockname()
so_srv.listen()
print("ecoute sur le port {}".format(adr_srv[1]))
while True:
    so_cli, adr_cli = so_srv.accept()
    nb_conn += 1
    th_jeu = th.Thread(target=jouer_avec, name=str(nb_conn),
                       args=(so_cli, adr_cli))
    try:
        th_jeu.start()
    except Exception as exc:
        print("{} sur la connexion avec {}".format(adr_cli))

so_srv.close()
exit(0)
