import  threading as th
from os import sched_yield

politesse = 50 # augmenter pour constater plus facilement le phénomène
nb_theads = 2
partagee = 0

def lit_puis_incremente_p_2_fois():
    global partagee
    for i in range(2):
        v = partagee                 # lire la valeur partagee
        print("thread {} : partagee = {}".format\
              (th.current_thread().name, partagee))        
        for _ in range(politesse):
            sched_yield()
        partagee = v + 1             # incrémente la valeur p
        

if __name__ == "__main__":
    # un seul thread : séquentiel
    lit_puis_incremente_p_2_fois()
    lit_puis_incremente_p_2_fois()
    print("maintenant, partagee = %i" % partagee)

    # deux threads : concurrent
    partagee = 0
    TH = [th.Thread(target=lit_puis_incremente_p_2_fois, name = str(i)) for i in
          range(nb_theads)]
    for t in TH: t.start()
    for t in TH: t.join()
    print("maintenant, partagee = %i" % partagee)
