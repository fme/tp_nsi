"""
Création d'une clé RSA, chiffrement et déchifrement d'un message
NB : ce sera Crypto et non Cryptodome avec les postes windows
et avec capytale
voir : 
- pour une belle explication : 
https://interstices.info/nombres-premiers-et-cryptologie-lalgorithme-rsa/
- et pour un petit défi :
https://www.spoj.com/problems/CERI2018G/
"""

import Cryptodome
from Cryptodome.Util.number import inverse, getPrime, size
from os import urandom
from time import localtime
from math import ceil, floor

bits = 128 # essayer avec davantage
msg = "vive la nsi"

p = getPrime(bits, urandom)
q = getPrime(bits, urandom)

n = p * q
phi_n = (p-1) * (q-1)

e = 2 ** 16 + 1  # bien s'assurer que gcd(e, phi_n) == 1
d = inverse(e, phi_n)

m = int.from_bytes(msg.encode('utf-8'), "little")
# voir explications en classe pour le boutisme

chiffré = pow(m, e, n)
déchiffré = pow(chiffré, d, n)

print(int.to_bytes(déchiffré, ceil(size(déchiffré)/8), "little"))
