"""
En passant par base64 pour la transmission
"""

import Cryptodome
from Cryptodome.Util.number import inverse, getPrime, size
from os import urandom
from time import localtime
from math import ceil, floor
import base64


BOM = "big" # byte order

# A crée ses clés
bits = 128 
p = getPrime(bits, urandom)
q = getPrime(bits, urandom)
A_n = p * q
phi_n = (p-1) * (q-1)
e = 2 ** 16 + 1                 # gcd(e, phi_n) == 1
A_d = inverse(e, phi_n)
# fin création clé

# A encode sa kpub (seulement n, puisque e est tj le m^ dans ce TP)
A_n_encodee = base64.b64encode(int.to_bytes(A_n, ceil(size(A_n)/8), BOM))

# A  envoie A_n_encodee qui est donc une str ...
# B décode A_n_encodee --> récupère le n de A, sous forme de bytes
n_recu_bytes = base64.b64decode(A_n_encodee)
n_recu_int = int.from_bytes(n_recu_bytes, BOM)
# test
assert n_recu_int == A_n, "échec transmissin A_n"

# B chiffre son message pour A grâce à la Kpub de A
# on écrit le message en ASCII sous forme de bytes pour éviter l'étape encodage
# utf-8
clair_B_pour_A = b"message de B pour A"
clair_B_pour_A_int = int.from_bytes(clair_B_pour_A, BOM)
chiffree = pow(clair_B_pour_A_int, e, n_recu_int)
chiffree_encodee = base64.b64encode(int.to_bytes(chiffree,
                                                ceil(size(chiffree)/8), BOM))
# B envoie  chiffree_encodee à A
# A b64décode le message --> bytes
chifree_decodee = base64.b64decode(chiffree_encodee)
# A transforme les bytes reçus en int
chifree_recue_int = int.from_bytes(chifree_decodee, BOM)
# A déchiffre l'int reçu à l'aide de sa clé privée (A_n, A_d)
dechifree_recue_int = pow(chifree_recue_int, A_d, A_n)
# contrôle
assert dechifree_recue_int == clair_B_pour_A_int, "échec test 2"
# A transforme l'int en bytes pour lire le message de B
dechifree_recue_bytes = int.to_bytes(dechifree_recue_int,
                                   ceil(size(dechifree_recue_int)/8),
                                   BOM)
print(dechifree_recue_bytes)
