"""
correction du tp chiffrement par transposition

dans la correction, je tiens compte du cas où la longueur du message
à chiffrer n'est pas un multiple de la longueur de la clé. Dans ce cas, 
certaines colonnes vont être plus longues que d'autres. Par ex chiffrer
"tout est perdu" avec la clé [2, 0, 1, 4, 3]

    0  1  2  3  4
    t  o  u  t  e
    s  t  p  e  r
    d  u

    les colonnes 2, 3 et 4 sont plus courtes

On utlise la fonction ceil pour calculer le nombre de lignes. 

ceil(x) renvoie le plus petit entier supérieure ou égal à x

par ex ceil(3.7) = 4
       ceil(3.0) == 3
"""
from math import ceil

# 1ère solution : avec une liste à 2 dimensions
def chiffrer_v1(claire: str, cle: list):
    """
    renvoie la version chiffrée par transposition du message
    claire selon la cle fournie
    """
    nbcol = len(cle)
    nblig = ceil(len(claire) / nbcol)
    tab = [[""] * nbcol for _ in range(nblig)]
    # remplir le tableau avec la version claire
    for i in range(len(claire)):
        tab[ i // nbcol ][ i % nbcol ] = claire[ i ]
    # écrire la version chiffrée
    chiffree = ""
    for k in range(len(cle)):
        for i in range(nblig):
            chiffree += tab[ i ][ cle[ k ] ]
    return chiffree


# Une autre solution : on divise le problème
def ecrire_colonne(claire, nblig, nbcol, ncol):
    colonne = ""
    for i in range(nblig):
        if i * nbcol + ncol < len(claire):
            colonne += claire[ i * nbcol + ncol ]
    return colonne

def chiffrer_v2(claire, cle):
    nbcol = len(cle)
    nblig = ceil(len(claire) / nbcol)
    chiffree = ""
    for k in range(len(cle)):
        chiffree += ecrire_colonne(claire, nblig, nbcol, cle[ k ])
    return chiffree

"""
DÉCHIFFRER, dans le cas particulier où la longueur du message est un multiple de
la longueur de la clé 

On veut reconstituer le tableau qui a servi à chiffrer 

    0 	1 	2 	3 	4
0   s 	o 	u 	s 	l
1   e 	p 	o 	n 	t
2   m 	i 	r 	a 	b
3   e 	a 	u 	c 	o
4   u 	l 	e 	l 	a
5   s 	e 	i 	n 	e

Pour chaque lettre du message chiffré, on écrit la ligne et la colonne
où on doit le retrouver 

     0  1  2  3  4  5  6  7  8  9  10 11 12 ...    
     u  o  r  u  e  i  s  e  m  e  u  s  o   ... pialeltboaesnacln

lig  0  1  2  3  4  5  0  1  2  3  4  5  0   ...
col  2  2  2  2  2  2  0  0  0  0  0  0  1   ...

On obtient le numéro de ligne en prenant le reste d'une division par 6 (nombre
de lignes).

Les numéros de colonne sont 2 (à 6 reprises), puis 0 (à 6 reprises), puis 1 ... 
dans l'ordre de la clé donc cle[ 0 ], cle[ 1 ] ....

On obtient l'indice dans la clé en divisant par 6 l'indice dans le message
chiffré 
"""


def dechiffrer(chiffree, cle):
    """
    fonctionne dans le cas particulier où la longueur du
    message est un multiple de la longueur de la clé.
    """
    nbcol = len(cle)
    nblig = ceil(len(chiffree) / nbcol)
    tab = [[""] * nbcol for _ in range(nblig)]
    # on reconstitue le tableau qui avait servi à chiffrer
    for i in range(len(chiffree)):
        tab[ i % nblig ][cle [ i // nblig ]] = chiffree[ i ]
    dechifree = ""
    for i in range(nblig):
        for j in range(nbcol):
            dechifree += tab[ i ][ j ]
    return dechifree

# version qui fonctionne dans tous les cas
def dechiffrer_general(chiffree, cle):
    # il faut trouver quelles étaient les colonnes plus hautes dans le tableau
    # qui a servi à chiffrer
    nbcol = len(cle)
    nblig = ceil(len(chiffree) / len(cle))
    hautes = nbcol - (nblig * nbcol - len(chiffree))

    tab = [[""] * nbcol for _ in range(nblig)]
    k = 0
    for i in range(len(cle)):
        htcol = nblig if cle[ i ] < hautes else nblig - 1
        for j in range(htcol):
            tab[ j ][cle[ i ]] = chiffree[ k ]
            k += 1
    # on n'a plus qu'à aplatir tab
    dechifree = ""
    for i in range(nblig):
        for j in range(nbcol):
            dechifree += tab[ i ][ j ]
    return dechifree


if __name__ == "__main__":
    msg1, cle1 = "souslepontmirabeaucoulelaseine", [2, 0, 1, 4, 3]
    assert chiffrer_v1(msg1, cle1) == "uorueisemeusopialeltboaesnacln"
    msg2 = "toutestperdu"
    assert chiffrer_v1(msg2, cle1) == "uptsdotuerte"
    assert chiffrer_v2(msg1, cle1) == "uorueisemeusopialeltboaesnacln"
    assert chiffrer_v2(msg2, cle1) == "uptsdotuerte"

    assert dechiffrer(chiffrer_v1(msg1, cle1), cle1) == msg1
    assert dechiffrer_general(chiffrer_v1(msg2, cle1), cle1) == msg2

    msg3 = "emportemoiwagonenlèvemoifrégate"
    cle2 = [3, 1, 2, 0, 4, 5]

    assert dechiffrer_general(chiffrer_v1(msg3, cle2), cle2) == msg3

    msg4 = "nousétionsàlétudequandleproviseurentrasuividunnouveauhabillé"+\
            "enbourgeoisetdungarçondeclassequiportaitungrandpupitre"
    assert dechiffrer_general(chiffrer_v1(msg4, cle2), cle2) == msg4


